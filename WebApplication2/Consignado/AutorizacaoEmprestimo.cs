﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    public class AutorizacaoEmprestimo
    {
        [XmlElement("autorizado")]
        public string Autorizado { get; set; }
        [XmlElement("dtValidade")]
        public string DataValidade { get; set; }
    }
}
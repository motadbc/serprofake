﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    public class ConsignadoContrato
    {
        [XmlElement("dtOperacao")]
        public DateTime DataOperacao { get; set; }
        [XmlElement("contrato")]
        public List<Contrato> Contrato { get; set; }
        [XmlElement("cdRetCode")]
        public string CodigoRetorno { get; set; }
        [XmlElement("dsRetCode")]
        public string DescricaoRetorno { get; set; }
    }
}
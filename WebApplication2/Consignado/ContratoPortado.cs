﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    public class ContratoPortado
    {
        [XmlElement("nrCnpj")]
        public string Cnpj { get; set; }
        [XmlElement("nmConsignataria")]
        public string NomeConsignataria { get; set; }
        [XmlElement("nrContrato")]
        public string Contrato { get; set; }
    }
}
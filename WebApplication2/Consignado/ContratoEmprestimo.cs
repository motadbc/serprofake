﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    [XmlRoot(Namespace = "")]
    public class ContratoEmprestimo
    {
        [XmlElement("response")]
        public ConsignadoContrato ConsignadoContrato { get; set; }
    }
}
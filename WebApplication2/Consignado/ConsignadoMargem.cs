﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;


namespace WebApplication2.Consignado
{
    public class ConsignadoMargem
    {
        [XmlElement("dtOperacao")]
        public DateTime DataOperacao { get; set; }
        [XmlElement("nome")]
        public string Nome { get; set; }
        [XmlElement("vinculoFuncional")]
        public List<VinculoFuncional> VinculoFuncional { get; set; }
        [XmlElement("cdRetCode")]
        public string CodigoRetorno { get; set; }
        [XmlElement("dsRetCode")]
        public string DescricaoRetorno { get; set; }
    }
}
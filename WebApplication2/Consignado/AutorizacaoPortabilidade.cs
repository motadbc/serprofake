﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    public class AutorizacaoPortabilidade
    {
        [XmlElement("autorizado")]
        public string Autorizado { get; set; }
        [XmlElement("dtValidade")]
        public string DataValidade { get; set; }
        [XmlElement("contratoPortado")]
        public ContratoPortado contratoPortado { get; set; }
        [XmlElement("vlMargemDisp")]
        public string ValorMargemDisponivel { get; set; }
    }
}
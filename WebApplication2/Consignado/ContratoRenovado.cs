﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    public class ContratoRenovado
    {
        [XmlElement("nrContrato")]
        public string Contrato { get; set; }
    }
}
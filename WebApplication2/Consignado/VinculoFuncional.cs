﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    public class VinculoFuncional
    {
        [XmlElement("codTipoVinc")]
        public string CodigoTipoVinculo { get; set; }
        [XmlElement("descTipoVinc")]
        public string DescricaoTipoVinculo { get; set; }
        [XmlElement("cdOrgao")]
        public string CodigoOrgao { get; set; }
        [XmlElement("nmOrgao")]
        public string NomeOrgao { get; set; }
        [XmlElement("cdMatricula")]
        public string CodigoMatricula { get; set; }
        [XmlElement("orgMatInst")]
        public string OrgaoMatriculaInstituicao { get; set; }
        [XmlElement("codClassificacao")]
        public string CodigoClassificacao { get; set; }
        [XmlElement("descClassificacao")]
        public string DescricaoClassificacao { get; set; }
        [XmlElement("cdUpag")]
        public string CodigoUpag { get; set; }
        [XmlElement("cdUfUpag")]
        public string CodigoUfUpag { get; set; }
        [XmlElement("produto")]
        public List<Produto> Produtos { get; set; }
    }
}
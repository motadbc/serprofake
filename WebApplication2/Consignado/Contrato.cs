﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    public class Contrato
    {
        [XmlElement("codTipoVinc")]
        public string CodigoTipoVinculo { get; set; }
        [XmlElement("descTipoVinc")]
        public string DescricaoTipoVinculo { get; set; }
        [XmlElement("cdOrgao")]
        public string CodigoOrgao { get; set; }
        [XmlElement("nmOrgao")]
        public string NomeOrgao { get; set; }
        [XmlElement("cdMatricula")]
        public string CodigoMatricula { get; set; }
        [XmlElement("orgMatInst")]
        public string OrgaoMatriculaInstituicao { get; set; }
        [XmlElement("cdRubrica")]
        public string CodigoRubrica { get; set; }
        [XmlElement("dsRubrica")]
        public string DescricaoRubrica { get; set; }
        [XmlElement("nrSequencia")]
        public string NumeroSequencia { get; set; }
        [XmlElement("cdConvenio")]
        public string CodigoConvenio { get; set; }
        [XmlElement("cdSituacao")]
        public string CodigoSituacao { get; set; }
        [XmlElement("dsSituacao")]
        public string DescricaoSituacao { get; set; }

        [XmlElement("nrContrato")]
        public string NumeroContrato { get; set; }

        [XmlElement("nrContratoRenovadoPor")]
        public string ContratoRenovadoPor { get; set; }

        [XmlElement("vlBruto")]
        public string ValorBruto { get; set; }
        [XmlElement("vlLiquido")]
        public string ValorLiquido { get; set; }
        [XmlElement("vlDesconto")]
        public string ValorDesconto { get; set; }
        [XmlElement("pzDesconto")]
        public string PrazoDesconto { get; set; }

        [XmlElement("txJurosMensal")]
        public string TaxaJurosMensal { get; set; }
        [XmlElement("iof")]
        public string Iof { get; set; }
        [XmlElement("cet")]
        public string Cet { get; set; }
        [XmlElement("dtInclusao")]
        public string dataInclusao { get; set; }
        [XmlElement("dtAnuencia")]
        public string dataAnuencia { get; set; }

        [XmlElement("contratosRenovados")]
        public List<ContratoRenovado> ContratosRenovados { get; set; }
        [XmlElement("contratoPortado")]
        public ContratoPortado ContratoPortado { get; set; }

        [XmlElement("dtEfetPortabilidade")]
        public string DataEfetivacaoPortabilidade { get; set; }

        [XmlElement("urlAceite")]
        public string UrlAceite { get; set; }
        [XmlElement("urlRecusa")]
        public string UrlRecusa { get; set; }
        [XmlElement("dtValidadeAnuencia")]
        public string DataValidadeAnuencia { get; set; }
        [XmlElement("dtInicioProcessoCip")]
        public string DataProcessoCip { get; set; }

        [XmlElement("anoMesPrimDesc")]
        public string AnoMesPrimeiroDesconto { get; set; }
        [XmlElement("anoMesUltDesc")]
        public string AnoMesUltimoDesconto { get; set; }

        [XmlElement("emailsParaNotificacaoAnuencia")]
        public List<EmailAnuencia> EmailsAnuencia { get; set; }
    }
}
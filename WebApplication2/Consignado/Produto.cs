﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    public class Produto
    {
        [XmlElement("cdRubrica")]
        public string CodigoRubrica { get; set; }
        [XmlElement("dsRubrica")]
        public string DescricaoRubrica { get; set; }
        [XmlElement("cdConvenio")]
        public string CodigoConvenio { get; set; }
        [XmlElement("vlMargemDisp")]
        public string ValorMargemDisponivel { get; set; }
        [XmlElement("autorizacaoEmprestimo")]
        public AutorizacaoEmprestimo autorizacaoEmprestimo { get; set; }
        [XmlElement("autorizacaoPortabilidade")]
        public AutorizacaoPortabilidade autorizacaoPortabilidade { get; set; }
        [XmlElement("autorizacaoCartao")]
        public AutorizacaoCartao autorizacaoCartao { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebApplication2.Consignado
{
    public class EmailAnuencia
    {
        [XmlElement("email")]
        public string Email { get; set; }
    }
}
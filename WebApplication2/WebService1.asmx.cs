﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml.Serialization;
using WebApplication2.Consignado;

namespace WebApplication2
{
    /// <summary>
    /// Descrição resumida de WebService1
    /// </summary>
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que esse serviço da web seja chamado a partir do script, usando ASP.NET AJAX, remova os comentários da linha a seguir. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Olá, Mundo";
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        public Consignataria ConsultarAutorizacoesMargemConsignavel(int cdConsig, string cdSenhaConsig, string nrCpf)
        {
            ConsignadoMargem consignado = new ConsignadoMargem();
            consignado.DataOperacao = DateTime.Now;
            consignado.Nome = "Fulano de Tal";
            consignado.CodigoRetorno = "0000";
            consignado.DescricaoRetorno = "Serviço realizado com sucesso.";

            List<Produto> produtos = new List<Produto>();
            Produto produto = new Produto()
            {
                CodigoRubrica = "34116",
                DescricaoRubrica = "EMPREST BCO OFICIAL - BCO BRAS",
                CodigoConvenio = "101",
                ValorMargemDisponivel = "100000",
                autorizacaoEmprestimo = new AutorizacaoEmprestimo()
                {
                    Autorizado = "S",
                    DataValidade = "20/11/2019"
                },
                autorizacaoPortabilidade = new AutorizacaoPortabilidade()
                {
                    Autorizado = "S",
                    DataValidade = "20/11/2019",
                    contratoPortado = new ContratoPortado()
                    {
                        Cnpj = "12345678901234",
                        NomeConsignataria = "CONSIGNATARIA XYZ",
                        Contrato = "XYZ111"
                    }
                }
            };
            Produto produto2 = new Produto()
            {
                CodigoRubrica = "34833",
                DescricaoRubrica = "AMORT CARTAO CREDITO - BCO BRAS",
                CodigoConvenio = "142",
                ValorMargemDisponivel = "100000",
                autorizacaoEmprestimo = new AutorizacaoEmprestimo()
                {
                    Autorizado = "S",
                    DataValidade = ""
                },
                autorizacaoCartao = new AutorizacaoCartao()
                {
                    Autorizado = "S",
                    DataValidade = ""
                }
            };
            produtos.Add(produto);
            produtos.Add(produto2);

            List<VinculoFuncional> vinculos = new List<VinculoFuncional>();
            vinculos.Add(new VinculoFuncional()
            { 
                CodigoTipoVinculo = "P", 
                DescricaoTipoVinculo = "Pensionista",
                CodigoOrgao = "17000",
                NomeOrgao = "MINISTERIO DA ECONOMIA",
                CodigoMatricula = "1234567",
                CodigoClassificacao = "1",
                DescricaoClassificacao = "Estável",
                CodigoUpag = "1700000000001",
                CodigoUfUpag = "DF",
                Produtos = produtos
            });

            consignado.VinculoFuncional = vinculos;

            Consignataria consignataria = new Consignataria()
            {
                ConsignadoMargem = consignado
            };
            return consignataria;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        public ContratoEmprestimo ConsultarContrato(int cdConsig, string cdSenhaConsig, string nrCpf, string nrContrato)
        {
            ContratoEmprestimo contratoEmprestimo = new ContratoEmprestimo();
            return contratoEmprestimo;
        }
    }
}
